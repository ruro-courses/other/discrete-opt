#!/bin/sh

mkdir -p output
texfot pdflatex \
    -interaction=nonstopmode \
    -halt-on-error \
    -synctex=1 \
    -file-line-error \
    -output-directory=output \
    -shell-escape \
    preview.tex \
    && echo -ne "\n====>  Done!  <====\n" \
    || echo -ne "\n====> Failed! <====\n"
